package com.devops.suma.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="suma")
public class SumaEntity {
	@Id
	private String clave;
	@NotNull
	private double op1;
	@NotNull
	private double op2;
	@Column
	private double resultado;
	
	public SumaEntity(){
		
	}
	
	public SumaEntity(String clave, double op1, double op2)
	{
		this.clave = clave;
		this.op1 = op1;
		this.op2 = op2;
		this.resultado = this.op1 + this.op2;
	}
	
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public double getOp1() {
		return op1;
	}
	public void setOp1(double op1) {
		this.op1 = op1;
	}
	public double getOp2() {
		return op2;
	}
	public void setOp2(double op2) {
		this.op2 = op2;
	}
	public double getResultado() {
		return resultado;
	}
	public void setResultado(double resultado) {
		this.resultado = resultado;
	}
	
	
	
	
}
