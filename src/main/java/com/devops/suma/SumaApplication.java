package com.devops.suma;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("com.devops.suma.data")
@EntityScan("com.devops.suma.data")
public class SumaApplication {

	 @Autowired
	 DataSource dataSource;
	
	public static void main(String[] args) {
		SpringApplication.run(SumaApplication.class, args);
	}

}
