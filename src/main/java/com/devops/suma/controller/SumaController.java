package com.devops.suma.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.devops.suma.services.SumaServices;

@Controller  
@RequestMapping(path="/demo")
public class SumaController {

	@Autowired
	private SumaServices service;
	
	@GetMapping("/sumar/{op1}/{op2}")
	public ResponseEntity<Object> sumar(@PathVariable(value="op1")double op1, @PathVariable(value="op2")double op2)
	{
		System.out.println("OP1: "+op1+", OP2: " +op2); 
		return new ResponseEntity<Object>(service.sumar(op1, op2), HttpStatus.OK);
		
	}

}
