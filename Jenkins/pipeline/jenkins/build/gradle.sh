#!/bin/bash

echo "* Building jar!*"

PROJ=/home/snake/Documents/retoDevops/docker-files/Jenkins/pipeline
docker run --rm -v /var/tmp -v $PROJ/java-app/suma:/app -w /app gradle "$@"

# argumento = ./gradlew clean assemble